package com.example.alunos.exemploactivity;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private int sort;
    private int cont = 0;
    private Random rand = new Random();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public int Inserir() {
        EditText palpite = findViewById(R.id.idValor);
        String v = palpite.getText().toString();
        int valor = Integer.parseInt(v);
        return valor;
    }

    public void Sortear(View v) {
        sort = rand.nextInt(1000);
        cont = 0;
    }


    public void Palpite(View v) {
        int value = Inserir();
        if (value == sort) {
            TextView res = findViewById(R.id.res);
            res.setText("Parabéns!! Você acertou!!");
            cont++;
            Intent i = new Intent(MainActivity.this, PlacarActivity.class);
            Bundle b = new Bundle();
            b.putInt("cont", cont);
            i.putExtras(b);
            startActivity(i);
        } else if (value <sort) {
            TextView res = findViewById(R.id.res);
            res.setText("Xiii... Palpite menor do que o sortedo Tente novamente!");
            cont++;
        } else {
            TextView res = findViewById(R.id.res);
            res.setText("Xiii... Palpite maior do que o sortedo Tente novamente!");
            cont++;
        }
    }

}
