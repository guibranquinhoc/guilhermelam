package com.example.alunos.exemploactivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class PlacarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_placar);

        Intent intencao = getIntent();
        Bundle pacote = intencao.getExtras();,

        if(pacote != null) {
            int tents = pacote.getInt("cont");
            SharedPreferences Placar = getPreferences(Context.MODE_PRIVATE);
            Context context = getApplicationContext();
            int recorde = Placar.getInt("recorde", 1000);
            int placar1 = Placar.getInt("placar1", 0);
            int placar2 = Placar.getInt("placar2", 0);
            int placar3 = Placar.getInt("placar3", 0);
            int placar4 = Placar.getInt("placar4", 0);
            int placar5 = Placar.getInt("placar5", 0);
            SharedPreferences.Editor editor = Placar.edit();
            if(tents < recorde) {
                editor.putInt("recorde", tents);
                recorde = tents;
            }
            placar5 = placar4;
            placar4 = placar3;
            placar3 = placar2;
            placar2 = placar1;
            placar1 = tents;
            editor.putInt("placar1", placar1);
            editor.putInt("placar2", placar2);
            editor.putInt("placar3", placar3);
            editor.putInt("placar4", placar4);
            editor.putInt("placar5", placar5);
            editor.commit();
            TextView r = findViewById(R.id.recorde);
            TextView text1 = findViewById(R.id.txt1);
            TextView text2 = findViewById(R.id.txt2);
            TextView text3 = findViewById(R.id.txt3);
            TextView text4 = findViewById(R.id.txt4);
            TextView text5 = findViewById(R.id.txt5);

            r.setText(Integer.toString(recorde));
            text1.setText(Integer.toString(placar1));
            text2.setText(Integer.toString(placar2));
            text3.setText(Integer.toString(placar3));
            text4.setText(Integer.toString(placar4));
            text5.setText(Integer.toString(placar5));

        }

    }
}