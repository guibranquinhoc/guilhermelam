package com.example.alunos.androidtoolbar;


import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.alunos.androidtoolbar.adapter.PessoaAdapter;
import com.example.alunos.androidtoolbar.model.Pessoa;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    public ArrayList<Pessoa> listaPessoas;
    private PessoaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        configureNavigationDrawer();
        configureToolbar();
        Fragment fr = new ViewsItensFragment();
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.frame, fr);
        t.addToBackStack(null);
        t.commit();
        listaPessoas = new ArrayList<>();
        adapter = new PessoaAdapter(listaPessoas, getApplicationContext());

    }

    private void configureToolbar() {
        Toolbar tb = findViewById(R.id.toolbar);
        setSupportActionBar(tb);
        ActionBar ab = getSupportActionBar();
        if (ab != null){
            ab.setHomeAsUpIndicator(R.mipmap.ic_launcher_round);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void configureNavigationDrawer() {
        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView nView = findViewById(R.id.navigation);
        nView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                        Fragment fr = null;
                        int itemId = menuItem.getItemId();

                        if(itemId == R.id.action_new_item) {
                            fr = new AddItemsFragment();
                        } else if (itemId == R.id.action_views_items) {
                            fr = new ViewsItensFragment();
                        }

                        if (fr != null) {
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.frame, fr);
                            transaction.commit();
                            drawerLayout.closeDrawers();
                            return true;
                        }
                        return false;
                    }
                });
    }
    @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.empty_menu, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int itemId = item.getItemId();

            switch (itemId) {
                // Android home
                case android.R.id.home:
                    drawerLayout.openDrawer(GravityCompat.START);
                    return true;

                // manage other entries if you have it ...
            }
            return true;
        }

        public ArrayList<Pessoa> getLista(){

        return this.listaPessoas;
        }
    public PessoaAdapter getAdapter(){

        return this.adapter;
    }
}
