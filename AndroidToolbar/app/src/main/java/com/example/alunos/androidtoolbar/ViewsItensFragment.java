package com.example.alunos.androidtoolbar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alunos.androidtoolbar.adapter.PessoaAdapter;
import com.example.alunos.androidtoolbar.model.Pessoa;

import java.util.ArrayList;

public class ViewsItensFragment extends Fragment {

    private ArrayList<Pessoa> listaPessoas;
    RecyclerView recy_v;
    PessoaAdapter pes_ad;

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        MainActivity atividade = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_view_items_layout, container, false);
        recy_v = view.findViewById(R.id.recy_view);
        listaPessoas = atividade.getLista();

        pes_ad = atividade.getAdapter();
        recy_v.setAdapter(pes_ad);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(atividade.getApplicationContext(), LinearLayoutManager.VERTICAL, false);

        recy_v.setLayoutManager(layout);


        return view;
    }
}
