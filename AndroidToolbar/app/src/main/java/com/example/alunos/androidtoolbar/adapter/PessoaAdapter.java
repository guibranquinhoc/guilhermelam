package com.example.alunos.androidtoolbar.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alunos.androidtoolbar.R;
import com.example.alunos.androidtoolbar.model.Pessoa;

import java.util.ArrayList;
import java.util.List;

public class PessoaAdapter extends RecyclerView.Adapter {

    private ArrayList<Pessoa> lista;
    private Context context;

    public PessoaAdapter(ArrayList<Pessoa> l, Context c){
        this.lista = l;
        this.context = c;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.item_lista,parent,false);
        return new PessoaViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        PessoaViewHolder theHolder = (PessoaViewHolder) holder;
        Pessoa pessoa = lista.get(i);
        theHolder.nome.setText(pessoa.getNome());
        theHolder.email.setText(pessoa.getEmail());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }
}
