package com.example.alunos.androidtoolbar.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Pessoa implements Parcelable {

    private String nome;
    private String email;

    public Pessoa(String n, String e){
        this.nome = n;
        this.email = e;
    }

    public String getNome(){
        return nome;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    protected Pessoa(Parcel in) {
        nome = in.readString();
        email = in.readString();
    }

    public static final Creator<Pessoa> CREATOR = new Creator<Pessoa>() {
        @Override
        public Pessoa createFromParcel(Parcel in) {
            return new Pessoa(in);
        }

        @Override
        public Pessoa[] newArray(int size) {
            return new Pessoa[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nome);
        dest.writeString(email);
    }
}
