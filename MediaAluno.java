import java.util.Scanner;

public class MediaAluno{
    public int[] notas = new int[4];

    public static void main(String[] args) {
        MediaAluno ma = new MediaAluno();
        ma.Inserir();
        ma.Media();
    }

    public void Inserir(){
        Scanner input = new Scanner(System.in);
        int a;
        for(int i = 0; i < 4; i++){
            a = input.nextInt();
            if((0 < a) && (a < 100)){
                notas[i] = a;
            }
        }
    }

    public void Media(){
        int media = 0;
        for(int i = 0; i < 4; i++){
            media += notas[i];
        }
        media = media/4;
        System.out.printf("Media = %d", media);
    }
}

