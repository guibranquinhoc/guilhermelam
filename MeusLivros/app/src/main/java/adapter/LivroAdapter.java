package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import model.Livro;

public class LivroAdapter extends RecyclerView.Adapter {
    private List<Livro> lista;
    private Context context;

    public LivroAdapter(List<Livro> l, Context c) {
        this.lista = l;
        this.context = c;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHOlder(
            @NonNull ViewGroup parent, int viewType
    ){
        View itemView = LayoutInflater.from(context)
        .inflate(R.layout.item_lista,parent,false);
        return new LivroViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(
            @NonNull RecyclerView.ViewHolder holder, int position
    ){
        LivroViewHolder theHolder = (LivroViewHolder) holder;
        Livro livro = lista.get(position);
        theHolder.titulo.setText(livro.getTitulo());
        theHolder.autor.setText(livro.getAutor());
        theHolder.descricao.setText(livro.getDescricao());
    }
}

    @Override
    public int getItemCount () {
        return lista.size();
    }
}

