package com.example.alunos.meuslivros;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private LivroViewModel viewModel;
    public static final int ADD_LIVRO_ACTIVITY_REQUEST_CODE = 1;
    public static final int EDIT_LIVRO_ACTIVITY_REQUEST_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        final LivroListAdapter adapter = new LivroListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        viewModel = ViewModelProviders.of(this).get(LivroViewModel.class);
        viewModel.getAllLivros().observe(this, new Observer<List<Livro>>(){
            @Override
            public void onChanged(@Nullable List<Livro> livros){
                adapter.setWords(livros);
            }
        });
    }

    public void addLivro(View v){
        Intent intent = new Intent(MainActivity.this, AddActivity.class);
        startActivityForResult(intent, ADD_LIVRO_ACTIVITY_REQUEST_CODE);
    }

    public LivroViewModel getLivroViewModel(){
        return viewModel;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ADD_LIVRO_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Livro livro = new Livro(0, data.getStringExtra(AddActivity.TITLE_REPLY),
                        data.getStringExtra(AddActivity.AUTOR_REPLY),
                        data.getStringExtra(AddActivity.EDITORA_REPLY));
                viewModel.insert(livro);
            } else {
                Toast.makeText(
                        getApplicationContext(),
                        R.string.empty_not_saved,
                        Toast.LENGTH_SHORT).show();

            }
        }

        if(requestCode == EDIT_LIVRO_ACTIVITY_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                Livro livro = data.getParcelableExtra("livro");
                viewModel.update(livro);
            }else{
                Toast.makeText(
                        getApplicationContext(),
                        R.string.empty_not_saved,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

}
