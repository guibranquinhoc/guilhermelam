import java.util.Scanner;

public class NumeroDecrescente{
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int x = input.nextInt();
		if(x > 0)
		 	while(x >= 0){
		 		System.out.printf(x + " ");
		 		x--;
		 	}
		 else
		 	System.out.println("0");
	}
}
