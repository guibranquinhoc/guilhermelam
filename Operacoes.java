import java.util.Scanner;

public class Operacoes{
	public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x, soma = 0, mult = 1, i;
        x = Integer.parseInt(input.next());
        for (i=1; i<x; i++){
            if(i%2 != 0){
                soma += i;
            }else{
                mult *= i;
            }
        }
        System.out.println(soma);
        System.out.println(mult);  
    }
}
